" be iMproved, required
set nocompatible       

" Light theme:
set background=dark

" required
filetype off                  

" set UTF-8 encoding
set enc=utf-8
set fenc=utf-8
set termencoding=utf-8

set ttimeoutlen=50

" disable vi compatibility (emulation of old bugs)
set nocompatible

" display ruler
set ruler
set laststatus=2

" use indentation of previous line
set autoindent

 " use intelligent indentation for C
set smartindent
set foldmethod=syntax
nnoremap <s-tab> za

" configure tabwidth and insert spaces instead of tabs
set tabstop=4        " tab width is 4 spaces
set shiftwidth=4     " indent also with 4 spaces
set expandtab        " expand tabs to spaces

" highlight end-column
highlight clear ColorColumn
set colorcolumn=80
hi CursorLine   cterm=NONE ctermbg=232 guibg=DarkRed
hi CursorColumn cterm=NONE ctermbg=232 guibg=darkred
set cursorline! cursorcolumn!

function! InsertStatuslineColor(mode)
  highlight ColorColumn ctermbg=5
  if a:mode == 'i'
    hi statusline guibg=Cyan ctermfg=9 guifg=White ctermbg=7
  elseif a:mode == 'r'
    hi statusline guibg=Purple ctermfg=5 guifg=Black ctermbg=0
  else
    hi statusline guibg=DarkRed ctermfg=1 guifg=Black ctermbg=0
  endif
endfunction

au InsertEnter * call InsertStatuslineColor(v:insertmode)
au InsertLeave * hi statusline guibg=DarkGrey ctermfg=8 guifg=White ctermbg=15
au InsertLeave * highlight clear ColorColumn

" default the statusline to green when entering Vim
hi statusline guibg=DarkGrey ctermfg=8 guifg=White ctermbg=15

" Formats the statusline
set statusline=%f                           " file name
set statusline+=[%{strlen(&fenc)?&fenc:'none'}, "file encoding
set statusline+=%{&ff}] "file format
set statusline+=%y      "filetype
set statusline+=%h      "help file flag
set statusline+=%m      "modified flag
set statusline+=%r      "read only flag

set statusline+=\ %=                        " align left
set statusline+=Line:%l/%L[%p%%]            " line X of Y [percent of file]
set statusline+=\ Col:%c                    " current column
set statusline+=\ Buf:%n                    " Buffer number
set statusline+=\ [%b][0x%B]\               " ASCII and byte code under cursor 

" Add subfolder seaching to path
set path+=**

" wildmenu
set wildmenu 

" code text width
" set textwidth=80

" turn syntax highlighting on
set t_Co=256
syntax on
" colorscheme wombat256

" turn line numbers on
set number

" highlight matching braces
set showmatch

" intelligent comments
set comments=sl:/*,mb:\ *,elx:\ */

" Enhanced keyboard mappings
inoremap jk <ESC>
nnoremap 1 ^
nnoremap 0 $

" clang-format
map <C-k> :pyf /usr/local/share/clang/clang-format.py<CR>

" copy-cut-paste
" set clipboard=unnamed

" use semicolon for colon
" nnoremap ; :
" nnoremap : ;

" backspace
set backspace=indent,eol,start

" 's'ingle character insert and back to normal mode 
nnoremap s :exec "normal i".nr2char(getchar())."\e"<CR>
nnoremap S :exec "normal a".nr2char(getchar())."\e"<CR>

" in normal mode F2 will save the file
nmap <F2> :w<CR>
" in insert mode F2 will exit insert, save, enters insert again
imap <F2> <ESC>:w<CR>i
" switch between header/source with F4
map <F4> :e %:p:s,.h$,.X123X,:s,.cpp$,.h,:s,.X123X$,.cpp,<CR>
" recreate tags file with F5
map <F5> :!ctags -R –c++-kinds=+p –fields=+iaS –extra=+q .<CR>
" create doxygen comment
map <F6> :Dox<CR>
" build using makeprg with <F7>
map <F7> :make<CR>
" build using makeprg with <S-F7>
map <S-F7> :make clean all<CR>
" goto definition with F12
map <F12> <C-]>
